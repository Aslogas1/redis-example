import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class Main {

    private static final Random random = new Random();

    public static void main(String[] args) throws InterruptedException {

        String key = "key";
        Jedis jedis = new Jedis();
        Stream.iterate(1, n -> n + 1)
                .limit(20).forEach(id -> jedis.zadd(key, id, "User" + id));
        int size = (int) jedis.zcard(key);
        while (true) {
            for (int i = 0; i <= size; i++) {
                int r = random.nextInt(size);

                String currentUser = jedis.zrange(key, 0, 0).iterator().next();
                String vip = "User" + r;
                System.out.printf("На главной странице показываем %s%n", currentUser);
                if (vip.equals(currentUser)) {
                    System.out.println("Пользователь " + vip + " оплатил платную услугу");
                }
                Thread.sleep(500);
                List<String> usersList = jedis.zrange(key, 0, -1);
                usersList.stream().limit(1).forEach(user -> jedis.zincrby(key, size - 1, user));
                usersList.stream().skip(1).forEach(user -> jedis.zincrby(key, -1, user));
            }
        }
    }
}
